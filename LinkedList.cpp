#include "LinkedList.h"


LinkedList::LinkedList():
	Cabeza(nullptr)
{

}

void LinkedList::InsertarInicio(const Pacha& pacha)
{
	Nodo* nuevonodo = new Nodo(pacha);
	if (ListaVacia())
		Cabeza = nuevonodo;
	else {
		nuevonodo->Siguiente = Cabeza;
		Cabeza = nuevonodo;
	}

}

Pacha LinkedList::RemoverPrimero()
{
	if (ListaVacia())
		throw ListaVaciaException{};
	Pacha resultado = Cabeza->Valor;
	Cabeza = Cabeza->Siguiente;
	return resultado;
}

Pacha LinkedList::VerPrimero()
{
	if (ListaVacia())
		throw ListaVaciaException{};
	return Cabeza->Valor;

}

bool LinkedList::ListaVacia() const
{
	return Cabeza == nullptr;
}
