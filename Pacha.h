#pragma once
#include <string>

class Pacha
{
private:

	std::string marca;
	int metanol;

public:

	void setmarca(std::string m);
	void setmetanol(int met);
	std::string getmarca();
	int getmetanol();
	Pacha(std::string m, int met);
	Pacha();

};

