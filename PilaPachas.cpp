#include "PilaPachas.h"

PilaPachas::PilaPachas()
{
    ListaPachas = new LinkedList();
}

void PilaPachas::Push(Pacha& const pacha)
{
    ListaPachas->InsertarInicio(pacha);
}

 Pacha PilaPachas::Pop()
{
	return ListaPachas->RemoverPrimero();
}

 Pacha PilaPachas::Peek()
 {
     return ListaPachas->VerPrimero();
 }

 bool PilaPachas::ListaVacia()
 {
	 return ListaPachas->ListaVacia();
 }
