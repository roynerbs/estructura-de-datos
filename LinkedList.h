#pragma once
#include "Pacha.h"
#include "Nodo.h"
#include "ListaVaciaException.h"
#include <string>

class LinkedList
{
private:
	Nodo* Cabeza;
public:
	LinkedList();
	void InsertarInicio(const Pacha& pacha);
	Pacha RemoverPrimero();
	Pacha VerPrimero();
	bool ListaVacia() const;

};

