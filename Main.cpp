#include <iostream>
#include "PilaPachas.h"
#include "Pacha.h"

int main()
{
	std::cout << "Bienvenido a la funcion de Pila" << std::endl;
	PilaPachas mipila{};
	Pacha pacha{};
	pacha.setmarca("marca");
	pacha.setmetanol(100);

	Pacha pacha2{};
	pacha2.setmarca("marca2");
	pacha2.setmetanol(100);

	mipila.Push(pacha);
	mipila.Push(pacha2);

	Pacha resultado = mipila.Peek();
	Pacha resultado2 = mipila.Pop();
	resultado = mipila.Pop();

}
