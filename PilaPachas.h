#pragma once
#include "Pacha.h"
#include "LinkedList.h"
class PilaPachas
{
private:
	LinkedList* ListaPachas;

public:
	PilaPachas();
	void Push(Pacha& const pacha);
	Pacha Pop();
	Pacha Peek();
	bool ListaVacia();
};

